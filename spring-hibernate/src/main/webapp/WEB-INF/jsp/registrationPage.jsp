<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration Form</title>
</head>
<body bgcolor="#ffe4c4">
  <center>
    <h2>Registration Form</h2>
    <form action="/registration-success" method="post">
    <table>
      <tr>
        <td>Name</td><td>:</td> <td><input type="text" name="name"/></td>
      </tr>
      <tr>
        <td>Designation</td><td>:</td> <td><input type="text" name="designation"/></td>
      </tr>
      <tr>
        <td>Username</td><td>:</td> <td><input type="text" name="username"/></td>
      </tr>
      <tr>
        <td>Password</td><td>:</td> <td><input type="password" name="password"/></td>
      </tr>
    </table>
    <input type="submit" value="Submit"/>
    <input type="reset" value="Reset"/>
  </form>
  </center>
</body>
</html>
