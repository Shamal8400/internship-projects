package com.spring.login.service;

import com.spring.login.bean.User;
import com.spring.login.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;

public class UserServiceImpl implements UserService
{
    @Autowired
    UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public int saveUserInfo(User user)
    {
        return userDao.saveUserInfo(user);
    }

    @Override
    public User login(User user)
    {
        return userDao.login(user);
    }

}
