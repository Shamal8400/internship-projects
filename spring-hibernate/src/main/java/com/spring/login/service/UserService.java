package com.spring.login.service;

import com.spring.login.bean.User;
import com.spring.login.dao.UserDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;

public interface UserService
{
    public int saveUserInfo(User user);
    public User login(User user);
}
