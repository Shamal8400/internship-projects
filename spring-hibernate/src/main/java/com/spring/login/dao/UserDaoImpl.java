package com.spring.login.dao;

import com.spring.login.bean.User;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Transactional(readOnly = false)
public class UserDaoImpl implements UserDao
{
    private static final Logger logger = LogManager.getLogger(UserDaoImpl.class);

    @Autowired

    SessionFactory sessionFactory;

    @Autowired
    HibernateTemplate hibernateTemplate;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Override
    public int saveUserInfo(User user)
    {
        return (int)hibernateTemplate.save(user);
    }

    @Override
    public User login(User user)
    {
        try(Session session = sessionFactory.openSession())
        {
            logger.info("IN UserDaoImpl.login() USERNAME IS={}, PASSWORD IS={}", user.getUsername(), user.getPassword());
            Query query = session.createQuery("SELECT name, designation FROM User WHERE username=:USERNAME AND password=:PASSWORD");
            query.setParameter("USERNAME", user.getUsername());
            query.setParameter("PASSWORD", user.getPassword());
            List<Object[]> list = query.list();
            String name=null;
            String designation=null;

            for(Object[] object:list)
            {
                logger.info("name is ={}", object[0]);
                logger.info("Designation is ={}", object[1]);

                name = (String)object[0];
                designation = (String)object[1];
            }
            user.setName(name);
            user.setDesignation(designation);
        }
        catch(Exception e)
        {
            logger.error("EXCEPTION IS RAISED= {}",e.getMessage());
        }
        return user;
    }
}
