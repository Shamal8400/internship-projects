package com.spring.login.dao;

import com.spring.login.bean.User;

public interface UserDao
{
    public int saveUserInfo(User user);
    public User login(User user);
}
