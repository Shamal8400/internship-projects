package com.spring.login.controller;

import com.spring.login.bean.User;
import com.spring.login.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController
{
    private static final Logger logger = LogManager.getLogger(LoginController.class);

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/login-form", method = RequestMethod.GET)
    public ModelAndView showLoginForm()
    {
        return new ModelAndView("login");
    }

    @RequestMapping(value = "/admin-home", method = RequestMethod.POST)
    public String loginAdmin(@ModelAttribute("user")User user, RedirectAttributes redirectAttributes)
    {
            logger.info("IN loginAdmin(), USERNAME IS ={}, PASSWORD IS={}", user.getUsername(), user.getPassword());
            User userAdmin = userService.login(user);
            if(userAdmin.getName() != null && userAdmin.getDesignation() != null && userAdmin.getDesignation().equalsIgnoreCase("Administrator"))
            {
                logger.info("IN Admin NAME IS ={}, DESIGNATION IS={}", user.getName(), user.getDesignation());
                return "administrator";
            }
            else if(userAdmin.getName() != null && userAdmin.getDesignation() != null && userAdmin.getDesignation().equalsIgnoreCase("Manager"))
            {
                logger.info("IN manager part NAME IS ={}, DESIGNATION IS={}", user.getName(), user.getDesignation());
                redirectAttributes.addFlashAttribute("userManager",user);
                return "redirect:manager-home";
            }
            else if(userAdmin.getName() != null && userAdmin.getDesignation() != null && userAdmin.getDesignation().equalsIgnoreCase("Employee"))
            {
                logger.info("IN manager part NAME IS ={}, DESIGNATION IS={}", user.getName(), user.getDesignation());
                redirectAttributes.addFlashAttribute("userEmp",user);
                return "redirect:employee-home";
            }
            else
            {
                return "redirect:login-fail";
            }
    }

    @RequestMapping(value = "/manager-home", method = RequestMethod.GET)
    public ModelAndView loginManager(@ModelAttribute("userManager")User user)
    {
        logger.info("IN loginManager()method, NAME IS ={}, DESIGNATION IS={}", user.getName(), user.getDesignation());
        ModelAndView modelAndView = new ModelAndView("manager");
        return modelAndView;
    }

    @RequestMapping(value = "/employee-home", method = RequestMethod.GET)
    public ModelAndView loginEmp(@ModelAttribute("userEmp")User user)
    {
        logger.info("IN loginEmp()method, NAME IS ={}, DESIGNATION IS={}", user.getName(), user.getDesignation());
        ModelAndView modelAndView = new ModelAndView("employee");
        return modelAndView;
    }

    @RequestMapping(value = "/login-fail", method = RequestMethod.GET)
    public String loginFailed()
    {
        return "loginFail";
    }
}
