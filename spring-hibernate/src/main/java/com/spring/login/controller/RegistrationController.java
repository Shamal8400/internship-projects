package com.spring.login.controller;

import com.spring.login.bean.User;
import com.spring.login.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegistrationController
{
    private static final Logger logger = LogManager.getLogger(RegistrationController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/registration-form", method = RequestMethod.GET)
    public ModelAndView showRegistrationForm()
    {
        return new ModelAndView("registrationPage");
    }

    @RequestMapping(value = "/registration-success", method = RequestMethod.POST)
    public ModelAndView saveRegistrationInfo(@ModelAttribute("users") User user)
    {
            int id= userService.saveUserInfo(user);
            if (id > 0)
            {
                return new ModelAndView("registrationSuccess", "successMessage", "Congratulation, Registration successfully completed...");
            }
            else
            {
                return new ModelAndView("registrationFailed", "errorMessage", "Registration failed...");
            }
        }
}
