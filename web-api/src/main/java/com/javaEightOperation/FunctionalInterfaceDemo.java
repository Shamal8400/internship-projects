package com.javaEightOperation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@FunctionalInterface
public interface FunctionalInterfaceDemo
{
    final Logger logger = LogManager.getLogger(FunctionalInterfaceDemo.class);
    void printData();
    default int add(int number1, int number2)
    {
        return number1 + number2;
    }
}
