package com.javaEightOperation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.postgresql.Driver;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.chrono.JapaneseDate;
import java.time.zone.ZoneRules;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

class JavaEightEnhancementDemo
{
    private static final Logger logger = LogManager.getLogger(JavaEightEnhancementDemo.class);

    private static List<String> printList()
    {
        List<String> namelist = new ArrayList<>();
        namelist.add("Rahul Lihare");
        namelist.add("Sumit Patil");
        namelist.add("Praful");
        namelist.add("Shubham");
        namelist.add("Atharva");
        namelist.add("Sumit Kambale");

        return namelist;
    }

    private static void parallelArraySort()
    {
        int[] array = {2,4,8,1,3,123,5,978,23,34};
        logger.info("Array element before sorting {}",array);
        Arrays.parallelSort(array);
        logger.info("Array element after sorting {}", array);

    }

    private static void getEncryptDecrypt()
    {
        byte[] byteValue = {11,22};
        Base64.Encoder encoder = Base64.getEncoder();
        byte[] encodeByte = encoder.encode(byteValue);
        logger.info("encoded valye :{}", encodeByte);
        String stringEncode = encoder.encodeToString("ENCODER".getBytes());
        logger.info("encryption of String is = '{}' ",stringEncode);

        Base64.Decoder decoder =  Base64.getDecoder();
        String stringValue = new String(decoder.decode(stringEncode));
        logger.info("{}", stringValue);

    }

    public void unnsignedArithmeticOperation()
    {
        int intvalue = Integer.parseUnsignedInt("4294967295");
        logger.info("UnsignedInt :{}",intvalue);
        String stringvalue = Integer.toUnsignedString(intvalue);
        logger.info("Unsigned Decimal Value:{}", stringvalue);
        int remainder = Integer.remainderUnsigned(5,2);
        logger.info("unsigned remainder is {}",remainder);
        int difference = Long.compareUnsigned(433366221,2343434);
        logger.info("{}", difference);
    }

    public static void main(String[] args)
    {
        FunctionalInterfaceDemo interfacedemo = () -> logger.info("This is example for Lambda Expression"); // Lambda Expression
        interfacedemo.printData();
        logger.info("Sum of two numbers in default method in interface :{}",interfacedemo.add(10,20));                              // default method in interface

        List<String> names = printList();
        names.forEach(logger::info);                                                                        // method refereneces
        names.stream().findFirst().ifPresent(logger::info);
        List<String> nameofpersons = names.stream().filter(name -> name.contains("Sumit")).collect(Collectors.toList());  // collection with stream()
        logger.info("Matching names to 'Sumit' in list is :{}", nameofpersons);
        List<String> sortedList = names.stream().sorted((name1, name2)-> name1.compareTo(name2)).collect(Collectors.toList());
        logger.info("Sorted Data :{}", sortedList);

        LocalDate date = LocalDate.now();                                                                 //DateAndTime
        logger.info("Current date is {}", date);
        LocalDate epochday = LocalDate.ofEpochDay(0);
        logger.info("Origin of the date for JAVA {} ",epochday);
        logger.info("{} {} {} {} ",date.getDayOfMonth(), date.getEra(),date.lengthOfMonth(), date.getChronology());

        LocalDateTime datetime = LocalDateTime.now();
        int time = datetime.getNano();
        logger.info("Time upto in milisecond {} ms", time);
        ZonedDateTime zonedate = ZonedDateTime.now();
        logger.info("ZoneDateAndTime is :{}, \n ZoneID is :{} ", zonedate, zonedate.getZone());

        JapaneseDate japan = JapaneseDate.now();
        logger.info("{}", japan);

        ZoneId zoneid = ZoneId.of("Asia/Calcutta");
        ZoneRules zonerule = zoneid.getRules();
        logger.info("ZoneRule Of New York {} ",zonerule);

        parallelArraySort();
        getEncryptDecrypt();
        JavaEightEnhancementDemo enhancementdemo = new JavaEightEnhancementDemo();
        enhancementdemo.unnsignedArithmeticOperation();

        logger.info("JDBCType vendor: {}",JDBCType.valueOf(0).getVendor());       //example of JDBCType
        logger.info("JDBCType type: {}",JDBCType.valueOf(-1).getName());

        Driver driver = new Driver();
        DriverAction action = () -> logger.info("Driver Deregister");                // DriverAction interface
        try
        {
            DriverManager.registerDriver(driver, action);
            try (Connection connection = DriverManager.getConnection("com.jdbc:postgresql://localhost:5432/internship_samples", "postgres", "shamalk");
                 Statement statement = connection.createStatement();
                 ResultSet result = statement.executeQuery("select * from users where id =6"))
            {
                while (result.next())
                {
                    logger.info("{} {} {} {} {} {}", result.getInt(1), result.getString(2), result.getString(3), result.getLong(4), result.getString(5), result.getString(6));
                }
                action.deregister();
            }
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage());
        }
    }
}
