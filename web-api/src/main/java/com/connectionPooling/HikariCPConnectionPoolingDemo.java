package com.connectionPooling;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Scanner;


public class HikariCPConnectionPoolingDemo
{
    private static final Logger logger = LogManager.getLogger(HikariCPConnectionPoolingDemo.class);

    static DataSource dataSource = HikariCPConnection.getDataSource();
    private int insertRecords() throws SQLException
    {
       try(Connection connection = dataSource.getConnection();
           PreparedStatement deletestatement = connection.prepareStatement("delete from users");
           PreparedStatement statement = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(?,?,?,?,?,?)"))
       {
           deletestatement.executeUpdate();
           statement.setInt(1,1);
           statement.setString(2,"Sumit");
           statement.setString(3, "sumit.kambale2014@gmail.com");
           statement.setLong(4,9011741198L);
           statement.setString(5, "sumit1");
           statement.setString(6,"sumit123");
           statement.addBatch();

           statement.setInt(1,2);
           statement.setString(2,"Shubham");
           statement.setString(3,"shubham_w@gmail.com");
           statement.setLong(4,9926457845L);
           statement.setString(5,"shubham1");
           statement.setString(6,"shubham123");
           statement.addBatch();

           statement.setInt(1,3);
           statement.setString(2,"Rohit");
           statement.setString(3, "rohit@yahoo.com");
           statement.setLong(4,7856456465L);
           statement.setString(5, "rohit1");
           statement.setString(6,"rohit123");
           statement.addBatch();

           statement.setInt(1,4);
           statement.setString(2,"Atharva");
           statement.setString(3, "atharv@rediffmail.com");
           statement.setLong(4,9845646464L);
           statement.setString(5, "atharv1");
           statement.setString(6,"atharv123");
           statement.addBatch();

           statement.setInt(1,5);
           statement.setString(2,"Ruchal");
           statement.setString(3, "b_ruchal@gmail.com");
           statement.setLong(4,8745645680L);
           statement.setString(5, "ruchal1");
           statement.setString(6,"ruchal123");
           statement.addBatch();

           statement.setInt(1,6);
           statement.setString(2,"Rahul");
           statement.setString(3, "rahul_g@gmail.com");
           statement.setLong(4,7895464312L);
           statement.setString(5, "rahul1");
           statement.setString(6,"rahul123");
           statement.addBatch();

           statement.setInt(1,7);
           statement.setString(2,"Abhishek");
           statement.setString(3, "abhishek@rediffmail.com");
           statement.setLong(4,8974563132L);
           statement.setString(5, "abhishek1");
           statement.setString(6,"abhishek123");
           statement.addBatch();

           statement.setInt(1,8);
           statement.setString(2,"Gaurav");
           statement.setString(3, "gaurav@rediffmail.com");
           statement.setLong(4,9894589032L);
           statement.setString(5, "gaurav1");
           statement.setString(6,"gauravs123");
           statement.addBatch();

           statement.setInt(1,9);
           statement.setString(2,"Raj");
           statement.setString(3, "raj@yahoo.com");
           statement.setLong(4,9894567132L);
           statement.setString(5, "raj1");
           statement.setString(6,"raj123");
           statement.addBatch();

           statement.setInt(1,10);
           statement.setString(2,"Karan");
           statement.setString(3, "karan@gmail.com");
           statement.setLong(4,9897563132L);
           statement.setString(5, "karan1");
           statement.setString(6,"karan123");
           statement.addBatch();
           int[] countRows = statement.executeBatch();
           int rows=0;
           for(int num =1; num<=countRows.length; num++)
           {
               rows = num;
           }
           return rows;
       }
    }

    private void getFirstRow() throws SQLException
    {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedstatement = connection.prepareStatement("select * from users fetch first 1 row only");
            ResultSet resultset = preparedstatement.executeQuery();)
        {
            while(resultset.next())
            {
                logger.info("{} {} {} {} {} {}", resultset.getInt(1), resultset.getString(2), resultset.getString(3), resultset.getLong(4), resultset.getString(5), resultset.getString(6));
            }
        }
    }

    private void getAllRecords() throws SQLException
    {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedstatement = connection.prepareStatement("select * from users");
            ResultSet resultset = preparedstatement.executeQuery())
        {
            while (resultset.next())
            {
                logger.info("{} {} {} {} {} {}", resultset.getInt(1), resultset.getString(2), resultset.getString(3), resultset.getLong(4), resultset.getString(5), resultset.getString(6));
            }
        }
    }

    private void updateFirstRecords() throws SQLException
    {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("update users set name=?, email=?, mobile_no=?, username=?, password=? where id in (select min(id) from users)"))
        {
            statement.setString(1,"Steve");
            statement.setString(2,"steve@gmail.com");
            statement.setLong(3, 9098505828L);
            statement.setString(4,"steve1");
            statement.setString(5,"steve123");
            int row = statement.executeUpdate();
            logger.info("{} row updated successfully...",row);
        }
    }

    private void updateAllRecords() throws SQLException
    {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement1 = connection.prepareStatement("update users set name=?, email=?, mobile_no=?, username=?, password=? where id =?");
            PreparedStatement statement2 = connection.prepareStatement("update users set name=?, email=?, mobile_no=?, username=?, password=? where id =?");
            PreparedStatement statement3 = connection.prepareStatement("update users set name=?, email=?, mobile_no=?, username=?, password=? where id =?");
            PreparedStatement statement4 = connection.prepareStatement("update users set name=?, email=?, mobile_no=?, username=?, password=? where id =?");
            PreparedStatement statement5 = connection.prepareStatement("update users set name=?, email=?, mobile_no=?, username=?, password=? where id =?");
            PreparedStatement statement6 = connection.prepareStatement("update users set name=?, email=?, mobile_no=?, username=?, password=? where id =?");
            PreparedStatement statement7 = connection.prepareStatement("update users set name=?, email=?, mobile_no=?, username=?, password=? where id =?");
            PreparedStatement statement8 = connection.prepareStatement("update users set name=?, email=?, mobile_no=?, username=?, password=? where id =?");
            PreparedStatement statement9 = connection.prepareStatement("update users set name=?, email=?, mobile_no=?, username=?, password=? where id =?");
            PreparedStatement statement10 = connection.prepareStatement("update users set name=?, email=?, mobile_no=?, username=?, password=? where id =?"))
        {
            statement1.setString(1,"Shamal");
            statement1.setString(2,"shamalk@gmail.com");
            statement1.setLong(3,7276138325L);
            statement1.setString(4,"shamal1");
            statement1.setString(5,"shamal123");
            statement1.setInt(6,1);
            statement1.executeUpdate();

            statement2.setString(1,"Divya");
            statement2.setString(2,"divyak@yahoo.com");
            statement2.setLong(3,9098282528L);
            statement2.setString(4,"divya1");
            statement2.setString(5,"divya123");
            statement2.setInt(6,2);
            statement2.executeUpdate();

            statement3.setString(1,"Ruchal");
            statement3.setString(2,"ruchal_b@gmail.com");
            statement3.setLong(3,9654658582L);
            statement3.setString(4,"ruchal1");
            statement3.setString(5,"ruchal123");
            statement3.setInt(6,3);
            statement3.executeUpdate();

            statement4.setString(1,"Raj");
            statement4.setString(2,"raj@rediffmail.com");
            statement4.setLong(3,7896546231L);
            statement4.setString(4,"raj1");
            statement4.setString(5,"raj123");
            statement4.setInt(6,4);
            statement4.executeUpdate();

            statement5.setString(1,"Rahul");
            statement5.setString(2,"rahul@gmail.com");
            statement5.setLong(3,8888784569L);
            statement5.setString(4,"rahul1");
            statement5.setString(5,"rahul123");
            statement5.setInt(6,5);
            statement5.executeUpdate();

            statement6.setString(1,"Nikita");
            statement6.setString(2,"nikita_k@yahoo.com");
            statement6.setLong(3,8795496875L);
            statement6.setString(4,"nikita1");
            statement6.setString(5,"nikita123");
            statement6.setInt(6,6);
            statement6.executeUpdate();

            statement7.setString(1,"Atharv");
            statement7.setString(2,"atharv@gmail.com");
            statement7.setLong(3,8789654696L);
            statement7.setString(4,"atharv1");
            statement7.setString(5,"atharv123");
            statement7.setInt(6,7);
            statement7.executeUpdate();

            statement8.setString(1,"Shubham");
            statement8.setString(2,"shubham@yahoo.com");
            statement8.setLong(3,7897946632L);
            statement8.setString(4,"shubham1");
            statement8.setString(5,"shubham123");
            statement8.setInt(6,8);
            statement8.executeUpdate();

            statement9.setString(1,"KK");
            statement9.setString(2,"kk@gmail.com");
            statement9.setLong(3,8978945612L);
            statement9.setString(4,"kk1");
            statement9.setString(5,"kk123");
            statement9.setInt(6,9);
            statement9.executeUpdate();

            statement10.setString(1,"Bhagyashree");
            statement10.setString(2,"payal_g@gmail.com");
            statement10.setLong(3,7984678996L);
            statement10.setString(4,"payal1");
            statement10.setString(5,"payal123");
            statement10.setInt(6,10);
            statement10.executeUpdate();
            logger.info("All rows updated successfully...");
        }
    }

    private void deleteFirstRow() throws SQLException
    {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("delete from users where id in(select min(id) from users)"))
        {
            int row = statement.executeUpdate();
            logger.info("first {} row deleted successfully...",row);
        }
    }

    private void deleteAllRecords() throws SQLException
    {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("delete from users"))
        {
            int rows = statement.executeUpdate();
            logger.info("All {} rows deleted successfully...",rows);
        }
    }
    public static void main(String[] args)
    {
        HikariCPConnectionPoolingDemo connectionPool = new HikariCPConnectionPoolingDemo();

        logger.info("Enter 'Insert Record' to insert record into database");
        logger.info("Enter 'Select First' for fetch first record from database");
        logger.info("Enter 'Select All' for fetch all records from database");
        logger.info("Enter 'Update First' to update First records into database");
        logger.info("Enter 'Update All' to update all records from database");
        logger.info("Enter 'Delete First' to delete first record from database");
        logger.info("Enter 'Delete All' to delete all records from database");
        try(Scanner scanner = new Scanner(System.in))
        {
            logger.info("Enter any 1 option from above :");
            String operation = scanner.nextLine();
            try
            {
                switch (operation)
                {
                    case "Insert Record" :
                        int rows = connectionPool.insertRecords();
                        logger.info("successful Insertion of All {} records into database....", rows);
                        break;
                    case "Select First" :
                        connectionPool.getFirstRow();
                        break;
                    case "Select All" :
                        connectionPool.getAllRecords();
                        break;
                    case "Update First" :
                        connectionPool.updateFirstRecords();
                        break;
                    case "Update All":
                        connectionPool.updateAllRecords();
                        break;
                    case "Delete First":
                        connectionPool.deleteFirstRow();
                        break;
                    case "Delete All":
                        connectionPool.deleteAllRecords();
                        break;
                    default:
                        logger.info("Invalid Input :{}",operation);
                }
            }
            catch(SQLException ex)
            {
                logger.error(ex);
            }
        }
    }
}
