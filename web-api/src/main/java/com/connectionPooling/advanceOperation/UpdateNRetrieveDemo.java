package com.connectionPooling.advanceOperation;

import com.connectionPooling.HikariCPConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.postgresql.Driver;

import javax.sql.DataSource;
import java.sql.*;

class UpdateNRetrieveDemo
{
    private static final Logger logger = LogManager.getLogger(UpdateNRetrieveDemo.class);
    public static void main(String[] args)
    {
        DataSource datasource = HikariCPConnection.getDataSource();
        Driver driver = new Driver();
        DriverAction action = () -> logger.info("deregister the driver");
        try
        {
            DriverManager.registerDriver(driver, action);
            try (Connection connection = datasource.getConnection();
                PreparedStatement preparedstatement = connection.prepareStatement("update users set username=? returning *"))
            {
                preparedstatement.setString(1, "steve1");
                try (ResultSet resultset = preparedstatement.executeQuery())
                {
                    while (resultset.next())
                    {
                        logger.info("{} {} {} {} {} {}", resultset.getInt(1), resultset.getString(2), resultset.getString(3), resultset.getLong(4), resultset.getString(5), resultset.getString(6));
                    }
                }
            }
            action.deregister();
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage());
        }
    }
}