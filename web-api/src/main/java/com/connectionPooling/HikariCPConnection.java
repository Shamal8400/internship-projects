package com.connectionPooling;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;

public class HikariCPConnection
{
    public static DataSource getDataSource()
    {
        HikariConfig config = new HikariConfig();
        config.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
        config.addDataSourceProperty("serverName", "localhost");
        config.addDataSourceProperty("portNumber", "5432");
        config.addDataSourceProperty("databaseName", "internship_samples");
        config.addDataSourceProperty("user", "postgres");
        config.addDataSourceProperty("password", "shamalk");

        config.setMaximumPoolSize(15);
        config.setConnectionTimeout(20000);
        config.setMaxLifetime(600000);

        return new HikariDataSource(config);
    }
}
