package com.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerDemo
{
    private static Logger logger = LogManager.getLogger(LoggerDemo.class);

    public static void main(String[] args)
    {
        Demo d = new Demo();
        d.print();
        logger.entry();
        logger.info("logger level :{}", logger.getLevel());
        logger.error(" This is an error.");
        logger.info("this is {}", "just information");
        logger.trace("Trace information");

    }
}
