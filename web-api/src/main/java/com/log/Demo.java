package com.log;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;

public class Demo
{
    //private static Logger com.log = LogManager.getRootLogger();
    private static Logger logger = LogManager.getLogger(Demo.class);

    public void print()
    {
        String msg = "Hello Sumit";
        int number =99;
        Date date = new Date();

//        LoggerDemo logdemo = new LoggerDemo();
//        logdemo.logMethods();
            logger.debug("Debug message without placeholder ");
            logger.debug("Debug message with Placeholder :{}, {}, {}", "Welcome", "to", "Maven", "Sumit");
            logger.entry();
            Level l = logger.getLevel();
            logger.info("logger level is :{}",l);
            logger.error("Error Message with Placeholder :{}, day :{}, date :{}, month :{}, year :{}", msg, date.getDay(), date.getDate(), date.getMonth(), date.getYear());
            logger.fatal("fatal....");
            logger.info("info message with placeholder:{}, Number :{}", msg, number);
            logger.warn(date.getMonth());
            logger.trace(date.getYear());
            logger.warn(Level.WARN);
    }
}
