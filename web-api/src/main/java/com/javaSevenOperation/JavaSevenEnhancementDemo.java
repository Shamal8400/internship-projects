package com.javaSevenOperation;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class JavaSevenEnhancementDemo
{
    private static Logger logger = LogManager.getLogger(JavaSevenEnhancementDemo.class);
    public static void getStringUsingSwitchStatement()
    {
        String lang = "Java";
        switch (lang)
        {
            case "C" :
                logger.info("This is C programming class");
                break;
            case "Java":
                logger.info("This is Java programming class");
                break;
            case "C++":
                logger.info("This is C++ programming class");
                break;
            case "Python":
                logger.info("This is Python programming class");
                break;
            default:
                logger.error("Invalid Input :{}",lang);
        }
    }

    private static ArrayList<Integer> inferTheTypeOfGenericInstance()
    {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(new Integer(1));
        arrayList.add(2);
        arrayList.add(10);
        return arrayList;
    }

    private static void printName()
    {
        try(Scanner scanner = new Scanner(System.in))
        {
            logger.info("Enter the value for name :");
            String name = scanner.next();
            logger.info("Entered name is :{}", name);
        }
    }

    public static void main(String[] args)
    {
        short binaryShort = 0b101_011;   //underscore used in binary value
        int binaryInt = 0B01111111111111111111111111111111;
        logger.info("Conversion of Binary value to Short value :{}", binaryShort);
        logger.info("Binary to decimal :{}",binaryInt);

        getStringUsingSwitchStatement();

        ArrayList<Integer> array = inferTheTypeOfGenericInstance();
        logger.info("Data of Generics version of ArrayList :{}",array);

        printName();

        try(Connection connection = MyConnection.getConnection())
        {
            logger.info("Connection created with :{}",connection);
        }
        catch (ClassNotFoundException | SQLException ex)  //Handling more than on type of exception
        {
            logger.error(ex.getMessage());
        }
    }
}
