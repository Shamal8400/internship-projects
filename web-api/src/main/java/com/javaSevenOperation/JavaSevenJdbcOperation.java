package com.javaSevenOperation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.sql.*;
import java.util.List;
import java.util.Scanner;

public class JavaSevenJdbcOperation
{
    private static Logger logger = LogManager.getLogger(JavaSevenJdbcOperation.class);

    private static void insertRecords() throws SQLException
    {
        try(Connection connection = DriverManager.getConnection("com.jdbc:postgresql://localhost:5432/internship_samples","postgres","shamalk");
            PreparedStatement statement1 = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(1,'Sumit','sumit.kambale2014@gmail.com',9011741198,'sumit1','sumit123')");
            PreparedStatement statement2 = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(2,'Shubham','shubham_w@gmail.com',9926457845,'shubham1','shubham123')");
            PreparedStatement statement3 = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(3,'Rohit','rohit@yahoo.com',7856456465,'rohit1','rohit123')");
            PreparedStatement statement4 = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(4,'Atharva','atharv@rediffmail.com',9845646464,'atharv1','atharv123')");
            PreparedStatement statement5 = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(5,'Ruchal','b_ruchal@gmail.com',8745645680,'ruchal1','ruchal123')");
            PreparedStatement statement6 = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(6,'Rahul','rahul_g@gmail.com',7895464312,'rahul1','rahul123')");
            PreparedStatement statement7 = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(7,'Abhishek','abhishek@rediffmail.com',8974563132,'abhi1','abhi123')");
            PreparedStatement statement8 = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(8,'Gaurav','gaurav@rediffmail.com',9876546325,'gaurav1','gaurav123')");
            PreparedStatement statement9 = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(9,'Raj','raj@yahoo.com',8987663132,'raj1','raj123')");
            PreparedStatement statement10 = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(10,'Karan','karan@gmail.com',9897563132,'karan1','karan123')"))
        {
            statement1.execute();
            statement2.execute();
            statement3.execute();
            statement4.execute();
            statement5.execute();
            statement6.execute();
            statement7.execute();
            statement8.execute();
            statement9.execute();
            statement10.execute();
        }
    }

    private static void getFirstRow() throws SQLException
    {
        try(Connection connection = DriverManager.getConnection("com.jdbc:postgresql://localhost:5432/internship_samples","postgres","shamalk");
            PreparedStatement statement = connection.prepareStatement("select * from users fetch first 1 row only");
            ResultSet resultset = statement.executeQuery())
        {
            while(resultset.next())
            {
                int id = resultset.getInt(1);
                String name = resultset.getString(2);
                String email = resultset.getString(3);
                long mobile_no = resultset.getLong(4);
                String username = resultset.getString(5);
                String password = resultset.getString(6);
                logger.info("Info. of First Row is :{}  {}  {}  {}  {}  {}",id, name, email, mobile_no, username, password);
            }
        }
    }
    private static void getAllRecords() throws SQLException
    {
        try(Connection connection = DriverManager.getConnection("com.jdbc:postgresql://localhost:5432/internship_samples","postgres","shamalk");
            PreparedStatement statement = connection.prepareStatement("select * from users");
            ResultSet resultset = statement.executeQuery())
        {
            while(resultset.next())
            {
                int id = resultset.getInt(1);
                String name = resultset.getString(2);
                String email = resultset.getString(3);
                long mobile = resultset.getLong(4);
                String username = resultset.getString(5);
                String password = resultset.getString(6);
                logger.info(" {} {} {} {} {} {}",id, name, email, mobile, username, password);
            }
        }
    }

    private static void updateFirstRecords() throws SQLException
    {
        try(Connection connection = DriverManager.getConnection("com.jdbc:postgresql://localhost:5432/internship_samples","postgres","shamalk");
            PreparedStatement statement = connection.prepareStatement("update users set name='Steve', email='steve@gmail.com', mobile_no=9098505828, username='steve1', password='steve123' where id in (select min(id) from users)"))
        {
            int row = statement.executeUpdate();
            logger.info("{} row updated successfully...",row);
        }
    }

    private static void updateAllRecords() throws SQLException {
        try(Connection connection = DriverManager.getConnection("com.jdbc:postgresql://localhost:5432/internship_samples","postgres","shamalk");
            PreparedStatement statement1 = connection.prepareStatement("update users set name='Shamal', email='shamalk@gmail.com', mobile_no=7276138325, username='shamal1', password='shamal123' where id =1");
            PreparedStatement statement2 = connection.prepareStatement("update users set name='Divya', email='divyak@yahoo.com', mobile_no=9098282528, username='divya1', password='divya123' where id =2");
            PreparedStatement statement3 = connection.prepareStatement("update users set name='Ruchal', email='ruchal_b@gmail.com', mobile_no=9654658582, username='ruchal1', password='ruchal123' where id =3");
            PreparedStatement statement4 = connection.prepareStatement("update users set name='Raj', email='raj@rediffmail.com', mobile_no=7896546231, username='raj1', password='raj123' where id =4");
            PreparedStatement statement5 = connection.prepareStatement("update users set name='Rahul', email='rahul@gmail.com', mobile_no=8888784569, username='rahul1', password='rahul123' where id =5");
            PreparedStatement statement6 = connection.prepareStatement("update users set name='Nikita', email='nikita_k@yahoo.com', mobile_no=8795496875, username='nika1', password='nik123' where id =6");
            PreparedStatement statement7 = connection.prepareStatement("update users set name='Atharva', email='atharv@gmail.com', mobile_no=8789654696, username='atharv1', password='atharv123' where id =7");
            PreparedStatement statement8 = connection.prepareStatement("update users set name='Shubham', email='shubham@yahoo.com', mobile_no=7897946632, username='shubham1', password='shubham123' where id =8");
            PreparedStatement statement9 = connection.prepareStatement("update users set name='KK', email='kk@gmail.com', mobile_no=8978945612, username='kk1', password='kk123' where id =9");
            PreparedStatement statement10 = connection.prepareStatement("update users set name='Bhabyashree', email='payal_g@gmail.com', mobile_no=7984678996, username='payal1', password='payal123' where id =10"))
        {
            statement1.executeUpdate();
            statement2.executeUpdate();
            statement3.executeUpdate();
            statement4.executeUpdate();
            statement5.executeUpdate();
            statement6.executeUpdate();
            statement7.executeUpdate();
            statement8.executeUpdate();
            statement9.executeUpdate();
            statement10.executeUpdate();
            logger.info("All rows updated successfully...");
        }

    }

    private static void deleteFirstRow() throws SQLException
    {
        try(Connection connection = DriverManager.getConnection("com.jdbc:postgresql://localhost:5432/internship_samples","postgres","shamalk");
            PreparedStatement statement = connection.prepareStatement("delete from users where id in(select min(id) from users)"))
        {
            int row = statement.executeUpdate();
            logger.info("first {} row deleted successfully...",row);
        }
    }
    private static void deleteAllRecords() throws SQLException
    {
        try(Connection connection = DriverManager.getConnection("com.jdbc:postgresql://localhost:5432/internship_samples","postgres","shamalk");
            PreparedStatement statement = connection.prepareStatement("delete from users"))
        {
            int rows = statement.executeUpdate();
            logger.info("All {} rows deleted successfully...",rows);
        }
    }

    public static void main(String[] args)
    {
        logger.info("Enter 'Insert Record' to insert record into database");
        logger.info("Enter 'Select First' for fetch first record from database");
        logger.info("Enter 'Select All' for fetch all records from database");
        logger.info("Enter 'Update First' to update First records into database");
        logger.info("Enter 'Update All' to update all records from database");
        logger.info("Enter 'Delete First' to delete first record from database");
        logger.info("Enter 'Delete All' to delete all records from database");
        try(Scanner scanner = new Scanner(System.in))
        {
            logger.info("Enter any 1 option from above :");
            String operation = scanner.nextLine();
            try
            {
                switch (operation)
                {
                    case "Insert Record" :
                        insertRecords();
                        logger.info("successful Insertion of All record into database....");
                        break;
                    case "Select First" :
                        getFirstRow();
                        break;
                    case "Select All" :
                        getAllRecords();
                        break;
                    case "Update First" :
                        updateFirstRecords();
                        break;
                    case "Update All":
                        updateAllRecords();
                        break;
                    case "Delete First":
                        deleteFirstRow();
                        break;
                    case "Delete All":
                        deleteAllRecords();
                        break;
                    default:
                        //throw new IllegalArgumentException("Invalid Input "+operation);
                        logger.info("Invalid Input :{}",operation);
                }
            }
            catch(SQLException ex)
            {
                logger.error(ex.getMessage());
            }
        }
    }
}
