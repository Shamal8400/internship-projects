package com.javaSevenOperation.advanceOperation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class UpdateNRetrieveDemo
{
    private static final Logger logger = LogManager.getLogger(UpdateNRetrieveDemo.class);
    public static void main(String[] args)
    {
        try(Connection connection = DriverManager.getConnection("com.jdbc:postgresql://localhost:5432/internship_samples","postgres","shamalk");
            PreparedStatement preparedstatement = connection.prepareStatement("update users set username='Steve1' returning *");
            ResultSet resultset = preparedstatement.executeQuery())
        {
            while (resultset.next())
            {
                int id = resultset.getInt(1);
                String name = resultset.getString(2);
                String email = resultset.getString(3);
                long mobile = resultset.getLong(4);
                String username = resultset.getString(5);
                String password = resultset.getString(6);
                logger.info("{} {} {} {} {} {}", id, name, email, mobile, username, password);
            }
        }
        catch (SQLException e){
            logger.error(e.getMessage());
        }
    }
}