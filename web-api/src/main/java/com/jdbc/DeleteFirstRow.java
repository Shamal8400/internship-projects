package com.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeleteFirstRow
{
    private static final Logger logger = LogManager.getLogger(DeleteFirstRow.class);
    public static void main(String[] args)
    {
        Connection connection=null;
        PreparedStatement preparestatement = null;
        try
        {
            connection = MyConnection.getConnection();
            preparestatement = connection.prepareStatement("delete from users where id in(select min(id) from users)");
            int num = preparestatement.executeUpdate();
            logger.info("no of rows affected by query :{}",num);
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            logger.error(e.getMessage());
        }
        finally
        {
            try
            {
                if (preparestatement != null)
                {
                    preparestatement.close();
                }
            }
            catch (SQLException e)
            {
                logger.error(e.getMessage());
            }
             try
             {
                if(connection!=null)
                {
                    connection.close();
                }
            }
            catch (SQLException e)
            {
                logger.error(e.getMessage());
            }
        }
    }
}
