package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection
{
    public static Connection getConnection() throws SQLException, ClassNotFoundException {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection("com.jdbc:postgresql://localhost:5432/internship_samples","postgres","shamalk");
    }
}
