package com.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;


public class InsertRecords
{
    private static final Logger logger = LogManager.getLogger(InsertRecords.class);

    public static void main(String[] args)
    {
        Connection connection=null;
        PreparedStatement preparestatement=null;
        try
        {
            connection = MyConnection.getConnection();
            preparestatement = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(1,'Sumit','sumit.kambale2014@gmail.com',9011741198,'sumit1','sumit123')");
            preparestatement.execute();
            preparestatement = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(2,'Shubham','shubham_w@gmail.com',9926457845,'shubham1','shubham123')");
            preparestatement.execute();
            preparestatement = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(3,'Rohit','rohit@yahoo.com',7856456465,'rohit1','rohit123')");
            preparestatement.execute();
            preparestatement = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(4,'Atharva','atharv@rediffmail.com',9845646464,'atharv1','atharv123')");
            preparestatement.execute();
            preparestatement = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(5,'Ruchal','b_ruchal@gmail.com',8745645680,'ruchal1','ruchal123')");
            preparestatement.execute();
            preparestatement = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(6,'Rahul','rahul_g@gmail.com',7895464312,'rahul1','rahul123')");
            preparestatement.execute();
            preparestatement = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(7,'Abhishek','abhishek@rediffmail.com',8974563132,'abhi1','abhi123')");
            preparestatement.execute();
            preparestatement = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(8,'Gaurav','gaurav@rediffmail.com',9876546325,'gaurav1','gaurav123')");
            preparestatement.execute();
            preparestatement = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(9,'Raj','raj@yahoo.com',8987663132,'raj1','raj123')");
            preparestatement.execute();
            preparestatement = connection.prepareStatement("insert into users(id,name,email,mobile_no,username,password) values(10,'Karan','karan@gmail.com',9897563132,'karan1','karan123')");
            preparestatement.execute();
        }
        catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
        catch (SQLException e) {
            logger.error(e.getMessage());
        }
        finally
        {
            try {
                if (preparestatement != null)
                {
                    preparestatement.close();
                }
            }
            catch (SQLException e)
            {
                logger.error(e.getMessage());
            }
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException e)
            {
                logger.error(e.getMessage());
            }
        }
    }
}
