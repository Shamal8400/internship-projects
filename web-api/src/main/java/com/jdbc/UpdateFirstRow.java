package com.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UpdateFirstRow
{
    private static final Logger logger = LogManager.getLogger(UpdateFirstRow.class);
    public static void main(String[] args)
    {
        Connection connection = null;
        PreparedStatement preparedstatement=null;
        try
        {
            connection = MyConnection.getConnection();
            preparedstatement = connection.prepareStatement("update users set name='Steve', email='steve@gmail.com', mobile_no=9098505828, username='steve1', password='steve123' where id in (select min(id) from users)");
            int rows = preparedstatement.executeUpdate();
            logger.info("no. of rows affected : {}",rows);
        }
        catch (SQLException e){
            logger.error(e.getMessage());
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
        finally
        {
            try
            {
                if (preparedstatement != null)
                {
                    preparedstatement.close();
                }
            }
            catch (SQLException e)
            {
                logger.error(e.getMessage());
            }
            try
            {
                if(connection!=null)
                {
                    connection.close();
                }
            } catch (SQLException e)
            {
                logger.error(e.getMessage());
            }
        }
    }
}
