package com.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeleteAllRows
{
    private static final Logger logger = LogManager.getLogger(DeleteAllRows.class);

    public static void main(String[] args)
    {
        Connection connection=null;
        PreparedStatement preparestatement=null;
        try
        {
            connection = MyConnection.getConnection();
            preparestatement = connection.prepareStatement("delete from users");
            preparestatement.execute();
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            logger.error(e.getMessage());
        }
        finally
        {
            try
            {
                if (preparestatement != null) {
                    preparestatement.close();
                    logger.info("preparestatement closed....");
                }
            }
            catch (SQLException e)
            {
                logger.error(e.getMessage());
            }

            try
            {
                if(connection!=null){
                    connection.close();
                    logger.info("connection closed....");
                }
            }
            catch (SQLException e)
            {
                logger.error(e.getMessage());
            }
        }
    }
}
