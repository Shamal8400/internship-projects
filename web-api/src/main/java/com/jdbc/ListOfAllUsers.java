package com.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ListOfAllUsers
{
    private static final Logger logger = LogManager.getLogger(ListOfAllUsers.class);
    public static void main(String[] args)
    {
        Connection connection=null;
        PreparedStatement preparestatment=null;
        ResultSet resultset=null;
        try
        {
            connection = MyConnection.getConnection();
            preparestatment = connection.prepareStatement("select * from users");
            resultset = preparestatment.executeQuery();

            while(resultset.next())
            {
                int id = resultset.getInt(1);
                String name = resultset.getString(2);
                String email = resultset.getString(3);
                Long mobile = resultset.getLong(4);
                String username = resultset.getString(5);
                String password = resultset.getString(6);
                logger.info(" {} {} {} {} {} {}",id, name, email, mobile, username, password);
            }
        }
        catch (ClassNotFoundException e)
        {
            logger.error(e.getMessage());
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage());
        }
        finally
        {
            try {
                if (resultset != null)
                {
                    resultset.close();
                    logger.info("resultset closed");
                }
            }
            catch(SQLException e)
            {
                logger.error(e.getMessage());
                logger.error("value not available in resultset");
            }
            try {
                if(preparestatment!=null){
                    preparestatment.close();
                    logger.info("preparestatment closed");
                }
            }
            catch(SQLException e)
            {
                logger.error(e.getMessage());
            }
            try {
                if(connection!=null){
                    connection.close();
                    logger.info("connection closed");
                }
            }
            catch(SQLException e)
            {
                logger.error(e.getMessage());
            }
        }
    }
}
