package com.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UpdateAllRows {
    private static final Logger logger = LogManager.getLogger(UpdateAllRows.class);

    public static void main(String[] args)
    {
         Connection connection=null;
         PreparedStatement preparedstatement=null;
        try
        {
            connection= MyConnection.getConnection();
            preparedstatement = connection.prepareStatement("update users set name='Shamal', email='shamalk@gmail.com', mobile_no=7276138325, username='shamal1', password='shamal123' where id =1");
            preparedstatement.executeUpdate();

            preparedstatement = connection.prepareStatement("update users set name='Divya', email='divyak@yahoo.com', mobile_no=9098282528, username='divya1', password='divya123' where id =2");
            preparedstatement.executeUpdate();

            preparedstatement = connection.prepareStatement("update users set name='Ruchal', email='ruchal_b@gmail.com', mobile_no=9654658582, username='ruchal1', password='ruchal123' where id =3");
            preparedstatement.executeUpdate();

            preparedstatement = connection.prepareStatement("update users set name='Raj', email='raj@rediffmail.com', mobile_no=7896546231, username='raj1', password='raj123' where id =4");
            preparedstatement.executeUpdate();

            preparedstatement = connection.prepareStatement("update users set name='Rahul', email='rahul@gmail.com', mobile_no=8888784569, username='rahul1', password='rahul123' where id =5");
            preparedstatement.executeUpdate();

            preparedstatement = connection.prepareStatement("update users set name='Nikita', email='nikita_k@yahoo.com', mobile_no=8795496875, username='nika1', password='nik123' where id =6");
            preparedstatement.executeUpdate();

            preparedstatement = connection.prepareStatement("update users set name='Atharva', email='atharv@gmail.com', mobile_no=8789654696, username='atharv1', password='atharv123' where id =7");
            preparedstatement.executeUpdate();

            preparedstatement = connection.prepareStatement("update users set name='Shubham', email='shubham@yahoo.com', mobile_no=7897946632, username='shubham1', password='shubham123' where id =8");
            preparedstatement.executeUpdate();

            preparedstatement = connection.prepareStatement("update users set name='KK', email='kk@gmail.com', mobile_no=8978945612, username='kk1', password='kk123' where id =9");
            preparedstatement.executeUpdate();

            preparedstatement = connection.prepareStatement("update users set name='Bhabyashree', email='payal_g@gmail.com', mobile_no=7984678996, username='payal1', password='payal123' where id =10");
            preparedstatement.executeUpdate();

        }
        catch (ClassNotFoundException e) {
            logger.error("problem in Driver class :{}",e.getMessage());

        }
        catch (SQLException e) {
            logger.error("problem in sql : {}",e.getMessage());
        }
        finally
        {
            try
            {
                if (preparedstatement != null)
                {
                    preparedstatement.close();
                }
            }
            catch (SQLException e) {
                logger.error(e.getMessage());
            }
            try
            {
                if(connection!=null)
                {
                    connection.close();
                }

            } catch (SQLException e) {
                logger.error(e.getMessage());
            }
        }
    }
}
