package com.jdbc.advanceOperation;

import com.jdbc.MyConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class UpdateAndListOfUsers
{
    private static final Logger logger = LogManager.getLogger(UpdateAndListOfUsers.class);

    public static void main(String[] args)
    {
        Connection connection=null;
        PreparedStatement preparedstatement=null;
        ResultSet resultset=null;
        try
        {
            connection = MyConnection.getConnection();
            preparedstatement = connection.prepareStatement("update userss set username='Steve1' returning *");
            resultset = preparedstatement.executeQuery();
            while (resultset.next())
            {
                    int id = resultset.getInt(1);
                    String name = resultset.getString(2);
                    String email = resultset.getString(3);
                    long mobile = resultset.getLong(4);
                    String username = resultset.getString(5);
                    String password = resultset.getString(6);
                    logger.info("{} {} {} {} {} {}", id, name, email, mobile, username, password);
            }
        }
        catch (SQLException e) {
            logger.error(e.getMessage());
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
        finally {
            try {
                if (resultset != null) {
                    resultset.close();

                } else {
                    logger.error("resultset is :{}", resultset);
                }
            }
            catch (SQLException e)
            {
                logger.error(e.getMessage());
                logger.info("resultset not closed {}", e);
            }

            try {
                if (preparedstatement != null)
                {
                    preparedstatement.close();
                }
                else
                    {
                    logger.error("preparedstatement is :{}", preparedstatement);
                }
            }catch(SQLException e)
            {
                logger.error(e.getMessage());
                logger.info("preparestatment not closed {}", e);
            }
            try
            {
                if(connection!=null)
                {
                    connection.close();
                }
                else{
                    logger.error("connection is :{}", connection);
                }

            } catch (SQLException e) {
                logger.error(e.getMessage());
                logger.info("connection not closed {}", e);

            }
        }
    }
}
