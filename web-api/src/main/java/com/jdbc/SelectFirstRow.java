package com.jdbc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SelectFirstRow
{
    private static final Logger logger = LogManager.getLogger(SelectFirstRow.class);
    public static void main(String[] args)
    {
        Connection connection=null;
        PreparedStatement preparestatement=null;
        ResultSet resultset=null;
        try
        {
            connection = MyConnection.getConnection();
            preparestatement = connection.prepareStatement("select * from users fetch first 1 row only");
            resultset = preparestatement.executeQuery();
            while(resultset.next())
            {
                int id = resultset.getInt(1);
                String name = resultset.getString(2);
                String email = resultset.getString(3);
                long mobile_no = resultset.getLong(4);
                String username = resultset.getString(5);
                String password = resultset.getString(6);
                logger.info("{}  {}  {}  {}  {}  {}",id, name, email, mobile_no, username, password);
            }
       }
       catch (SQLException e){
            logger.error(e.getMessage());
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
        finally {
            try {
                if (resultset != null) {
                    resultset.close();
                }
            }
            catch (SQLException e) {
                logger.error(e.getMessage());
            }
            try {
                if (preparestatement != null) {
                    preparestatement.close();
                }
            }
            catch (SQLException e) {
                logger.error(e.getMessage());
            }
            try{
                if(connection!=null) {
                    connection.close();
                }

            } catch (SQLException e) {
                logger.error(e.getMessage());
            }
        }
    }
}
