package com.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloWorldServlet extends HttpServlet
{
    private final Logger logger = LogManager.getLogger(HelloWorldServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    {
        response.setContentType("text/html");           //GET Endpoint is==> http://localhost:8080/api/hello-world?username=Batman
        try(PrintWriter writer = response.getWriter())
        {
            String name = request.getParameter("username");
            logger.info("name is :{}", name);
            writer.print("<font size='5'><b>Hello</b></font>, "+name);
        }
        catch (IOException e)
        {
            logger.error(e.getMessage());
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    {
        // to call doPost() use attribute method="post" in <form> in index.html
        response.setContentType("text/html");                        // POST Endpoint is==>   http://localhost:8080/api/hello-world
        try (PrintWriter writer = response.getWriter())
        {
            String name = request.getParameter("username");
            writer.print("<Font size='6' color='red'><b>Hello</b>, "+name+"</font>");
        }
        catch (IOException e)
        {
            logger.error(e.getMessage());
        }
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response)
    {

    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
    {

    }
}
